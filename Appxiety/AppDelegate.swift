//
//  AppDelegate.swift
//  Appxiety
//
//  Created by trying-things on 01/07/2018.
//  Copyright © 2018 trying-things. All rights reserved.
//

import UIKit
import Firebase
import WebRTC
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate, MessagingDelegate {

    var window: UIWindow?
    var webRTC: WebRTC?
    
    let gcmMessageIDKey = "gcm.message_id"


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        // Setup Firebase
        FirebaseApp.configure()
        // Set AppDelegate as Firebase Cloud Messaging delegate
        Messaging.messaging().delegate = self
        // Receive data messages when app is in foreground
        Messaging.messaging().shouldEstablishDirectChannel = true
        FirebaseAuth.signInAnonymously()
        
        // Listen for notifications to start/close a WebRTC instance
        NotificationCenter.default.addObserver(self, selector: #selector(self.startWebRTC(_:)), name: .StartWebRTC, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.closeWebRTC(_:)), name: .CloseWebRTC, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.webRTC_Offer(_:)), name: .WebRTC_Offer, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.webRTC_Answer(_:)), name: .WebRTC_Answer, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.webRTC_Candidate(_:)), name: .WebRTC_Candidate, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.webRTC_Close(_:)), name: .WebRTC_Close, object: nil)
   
        // Register for push notifications
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()

        
        return true
    }
    
    @objc func startWebRTC(_ notification: Notification) {
        print("AppDelegate.startWebRTC(): Starting WebRTC instance")

        
        if webRTC == nil {
            print("AppDelegate.startWebRTC(): New WebRTC instance started")
            webRTC = WebRTC()
            if let webRTC = webRTC {
                webRTC.peerConnectionFactory = RTCPeerConnectionFactory()
                webRTC.startAudio()
                let firebase = Firebase.shared
                firebase.setup()
                if webRTC.peerConnection == nil {
                    print("make offer")
                    webRTC.makeOffer()
                } else {
                    print("peer already exists")
                }
            }
        } else {
            print("startWebRTC(): WebRTC already exists")
        }
    }
    
    @objc func closeWebRTC(_ notification: Notification) {
        print("AppDelegate.closeWebRTC(): Closing WebRTC instance")

        if webRTC == nil {
            print("AppDelegate.closeWebRTC(): WebRTC is already nil")
        } else {
            if let webRTC = webRTC {
                print("AppDelegate.closeWebRTC(): called WebRTC.close()")
                webRTC.close()
            }
            print("AppDelegate.closeWebRTC(): WebRTC instance is now nil")
            webRTC = nil
        }
    }

    @objc func webRTC_Offer(_ notification: Notification) {
        print("AppDelegate.webRTC_Offer() received offer")
        let offer = notification.object as! RTCSessionDescription
        if let webRTC = webRTC {
            print("AppDelegate.webRTC_Offer() sent offer to WebRTC.setOffer()")
            webRTC.setOffer(offer)
        }
    }
    
    @objc func webRTC_Answer(_ notification: Notification) {
        print("AppDelegate.webRTC_Answer() received answer")
        let answer = notification.object as! RTCSessionDescription
        if let webRTC = webRTC {
            print("AppDelegate.webRTC_Answer() sent answer to WebRTC.setAnswer()")
            webRTC.setAnswer(answer)
        }
    }
    
    @objc func webRTC_Candidate(_ notification: Notification) {
        print("AppDelegate.webRTC_Candidate() received candidate")
        let candidate = notification.object as! RTCIceCandidate
        if let webRTC = webRTC {
            print("AppDelegate.webRTC_andidateC() sent candidate to WebRTC.setCandidate()")
            webRTC.addIceCandidate(candidate)
        }
    }
    
    @objc func webRTC_Close(_ notification: Notification) {
        print("AppDelegate.webRTC_Close() received close notification")
        if let webRTC = webRTC {
            webRTC.close()
            let database = Firebase.shared
            database.stopNegotiatingConnection()
            FirebaseAuth.anxiousID = nil
            FirebaseAuth.helperID = nil
            database.callSender = nil
            database.callReceiver = nil
        }
    }


    
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        // TODO: Save to database with FirebaseAuth ID
        print("Firebase Cloud Messaging token: \(fcmToken)")
        FirebaseAuth.myPushToken = fcmToken
    }
    
    // Handle receiving data push notifications when app is in foreground
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        
        completionHandler(UIBackgroundFetchResult.newData)
    }



}

extension Notification.Name {
    static let StartWebRTC = Notification.Name("StartWebRTC")
    static let CloseWebRTC = Notification.Name("CloseWebRTC")
    static let WebRTC_Offer = Notification.Name("WebRTC_Offer")
    static let WebRTC_Answer = Notification.Name("WebRTC_Answer")
    static let WebRTC_Candidate = Notification.Name("WebRTC_Candidate")
    static let WebRTC_Close = Notification.Name("WebRTC_Close")
    static let WebRTC_Connected = Notification.Name("WebRTC_Connected")
}
