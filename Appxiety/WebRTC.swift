// Source: https://github.com/dangnguyenhuu/iOSWebRTC
//
//
//  WebRTC.swift
//  Appxiety
//
//  Created by trying-things on 01/07/2018.
//  Copyright © 2018 trying-things. All rights reserved.
//

import UIKit
import WebRTC
import Firebase
import SwiftyJSON

class WebRTC: NSObject {
    
    public static let shared = WebRTC()
    let firebase = Firebase.shared
    var peerConnectionFactory: RTCPeerConnectionFactory! = nil
    var peerConnection: RTCPeerConnection! = nil
    var audioSource: RTCAudioSource?
    
    deinit {
        if peerConnection != nil {
            hangUp()
        }
        audioSource = nil
        peerConnectionFactory = nil
        print("DEINIT")
        firebase.callReceiver?.removeAllObservers()
        FirebaseAuth.anxiousID = nil
        FirebaseAuth.helperID = nil
    }
    
    func close() {
        if peerConnection != nil {
            hangUp()
        }
        audioSource = nil
        peerConnectionFactory = nil
        print("close()")
        print("peerConnection = \(peerConnection.debugDescription)")
        firebase.callReceiver?.removeAllObservers()
    }
    
    func startAudio() {
        let audioSourceConstraints = RTCMediaConstraints(mandatoryConstraints: nil, optionalConstraints: nil)
        audioSource = peerConnectionFactory.audioSource(with: audioSourceConstraints)
    }
    
    func prepareNewConnection() -> RTCPeerConnection {
        let configuration = RTCConfiguration()
        configuration.iceServers = [RTCIceServer.init(urlStrings: ["stun:stun.l.google.com:19302", "stun:stun2.l.google.com:19302", "stun:stun3.l.google.com:19302", "stun:stun4.l.google.com:19302","turn:numb.viagenie.ca:3478"], username: "hello@tryingthings.com", credential: "quaintcurtain")]
        let peerConnectionConstraints = RTCMediaConstraints(
            mandatoryConstraints: nil, optionalConstraints: nil)
        let peerConnection = peerConnectionFactory.peerConnection(with: configuration, constraints: peerConnectionConstraints, delegate: self)
        let localAudioTrack = peerConnectionFactory.audioTrack(with: audioSource!, trackId: "ARDAMSa0")
        let audioSender = peerConnection.sender(withKind: kRTCMediaStreamTrackKindAudio, streamId: "ARDAMS")
        audioSender.track = localAudioTrack
        return peerConnection
    }
    
    func setOffer(_ offer: RTCSessionDescription) {
        
        
        //        APP_DELGATE.callManager?.receiveCall()
        print("here is where call would be received")
        
        
        self.peerConnection = prepareNewConnection() // PeerConnectionを生成する
        self.peerConnection.setRemoteDescription(offer, completionHandler: {(error: Error?) in
            if error == nil {
                print("setRemoteDescription(offer) succsess")
                self.makeAnswer() // setRemoteDescriptionが成功したらAnswerを作る
            } else {
                print("setRemoteDescription(offer) ERROR: " + error.debugDescription)
            }
        })
    }
    
    func setAnswer(_ answer: RTCSessionDescription) {
        if peerConnection == nil {
            print("peerConnection NOT exist!")
            return
        }
        
        self.peerConnection.setRemoteDescription(answer, completionHandler: { // 受け取ったSDPを相手のSDPとして設定
            (error: Error?) in
            if error == nil {
                print("setRemoteDescription(answer) succsess")
            } else {
                print("setRemoteDescription(answer) ERROR: " + error.debugDescription)
            }
        })
    }
    
    func addIceCandidate(_ candidate: RTCIceCandidate) {
        if peerConnection != nil {
            peerConnection.add(candidate)
        } else {
            print("PeerConnection not exist!")
        }
    }
    
    func makeOffer() {
        peerConnection = prepareNewConnection() // PeerConnectionを生成
        
        let constraints = RTCMediaConstraints(mandatoryConstraints: ["OfferToReceiveAudio": "true", "OfferToReceiveVideo": "true"],
                                              optionalConstraints: nil) // Offerの設定 今回は映像も音声も受け取る
        let offerCompletion = { (offer: RTCSessionDescription?, error: Error?) in // Offerの生成が完了した際の処理
            
            if error != nil { return }
            print("createOffer() succsess")
            let setLocalDescCompletion = {(error: Error?) in // setLocalDescCompletionが完了した際の処理
                
                if error != nil { return }
                print("setLocalDescription() succsess")
                
                self.sendSDP(offer!) // 相手に送る
            }
            
            self.peerConnection.setLocalDescription(offer!, completionHandler: setLocalDescCompletion) // 生成したOfferを自分のSDPとして設定
        }
        
        
        self.peerConnection.offer(for: constraints, completionHandler: offerCompletion) // Offerを生成
    }
    
    
    func makeAnswer() {
        print("sending Answer. Creating remote session description...")
        if peerConnection == nil {
            print("peerConnection NOT exist!")
            return
        }
        
        let constraints = RTCMediaConstraints(mandatoryConstraints: nil, optionalConstraints: nil)
        let answerCompletion = { (answer: RTCSessionDescription?, error: Error?) in
            if error != nil {
                print("answerCompletion error: \(error.debugDescription)")
                return
                
            }
            print("createAnswer() succsess")
            let setLocalDescCompletion = {(error: Error?) in
                if error != nil { return }
                print("setLocalDescription() succsess")
                
                self.sendSDP(answer!) // 相手に送る
            }
            self.peerConnection.setLocalDescription(answer!, completionHandler: setLocalDescCompletion)
        }
        
        self.peerConnection.answer(for: constraints, completionHandler: answerCompletion) // Answerを生成
    }
    
    
    
    func sendSDP(_ desc: RTCSessionDescription) {
        print("---sending sdp ---")
        
        let jsonSdp: JSON = [ // JSONを生成
            "sdp": desc.sdp, // SDP本体
            "type": RTCSessionDescription.string(for: desc.type) // offer か answer か
        ]
        let message = jsonSdp.dictionaryObject
        
        firebase.callSender?.setValue(message) { (error, ref) in // 相手に送信
            if error != nil {
                print("Dang sendIceCandidate -->> ", error.debugDescription)
            }
        }
    }
    
    func hangUp() {
        if peerConnection != nil {
            if peerConnection.iceConnectionState != RTCIceConnectionState.closed {
                peerConnection.close()
                let jsonClose: JSON = ["type": "close"]
                
                let message = jsonClose.dictionaryObject
                print("sending close message")
                let ref = Database.database().reference().child("Call/\(firebase.sender)")
                ref.setValue(message) { (error, ref) in
                    print("Dang send SDP Error -->> ", error.debugDescription)
                }
                
            }
            peerConnection = nil
            print("peerConnection is closed.")
            
            
        } else {
            print("hangup thinks peerConnection is nil")
        }
        FirebaseAuth.anxiousID = nil
        FirebaseAuth.helperID = nil
    }
    
    
}

// MARK: - Peer Connection
extension WebRTC: RTCPeerConnectionDelegate {
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didChange stateChanged: RTCSignalingState) {
        // 接続情報交換の状況が変化した際に呼ばれます
        print("\(#function): 接続情報交換の状況が変化した際に呼ばれます")
    }
    
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didAdd stream: RTCMediaStream) {
        // 映像/音声が追加された際に呼ばれます
        print("-- peer.onaddstream()")
        //        DispatchQueue.main.async(execute: { () -> Void in
        //            // mainスレッドで実行
        //            //            if (stream.videoTracks.count > 0) {
        //            //                // ビデオのトラックを取り出して
        //            //                self.remoteVideoTrack = stream.videoTracks[0]
        //            //                // remoteVideoViewに紐づける
        //            //                self.remoteVideoTrack?.add(self.remoteVideoView)
        //            //            }
        //        })
    }
    
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didRemove stream: RTCMediaStream) {
        // 映像/音声削除された際に呼ばれます
    }
    
    
    func peerConnectionShouldNegotiate(_ peerConnection: RTCPeerConnection) {
        // 接続情報の交換が必要になった際に呼ばれます
    }
    
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didChange newState: RTCIceConnectionState) {
        // PeerConnectionの接続状況が変化した際に呼ばれます
        var state = ""
        switch (newState) {
        case RTCIceConnectionState.checking: state = "checking"
        case RTCIceConnectionState.completed: state = "completed"
        case RTCIceConnectionState.connected:
            state = "connected"
            NotificationCenter.default.post(name: .WebRTC_Connected, object: nil)
        case RTCIceConnectionState.closed:
            state = "closed"
            hangUp()
        case RTCIceConnectionState.failed:
            state = "failed"
            hangUp()
        case RTCIceConnectionState.disconnected: state = "disconnected"
        default: break
        }
        print("ICE connection Status has changed to \(state)")
    }
    
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didChange newState: RTCIceGatheringState) {
        // 接続先候補の探索状況が変化した際に呼ばれます
    }
    
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didGenerate candidate: RTCIceCandidate) {
        // Candidate(自分への接続先候補情報)が生成された際に呼ばれます
        if candidate.sdpMid != nil {
            sendIceCandidate(candidate)
        } else {
            print("empty ice event")
        }
    }
    
    
    func sendIceCandidate(_ candidate: RTCIceCandidate) {
        print("---sending ICE candidate ---")
        let jsonCandidate: JSON = ["type": "candidate",
                                   "ice": [
                                    "candidate": candidate.sdp,
                                    "sdpMLineIndex": candidate.sdpMLineIndex,
                                    "sdpMid": candidate.sdpMid!
            ]
        ]
        
        let message = jsonCandidate.dictionaryObject
        
        firebase.callSender?.setValue(message) { (error, ref) in
            if error != nil {
                print("Dang sendIceCandidate -->> ", error.debugDescription)
            }
        }
    }
    
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didOpen dataChannel: RTCDataChannel) {
        print("Opened data channel")
    }
    
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didRemove candidates: [RTCIceCandidate]) {
        print("Closed data channel for \(candidates)")
    }
    
}
