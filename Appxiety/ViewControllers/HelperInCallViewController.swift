//
//  HelperInCallViewController.swift
//  Appxiety
//
//  Created by trying-things on 01/07/2018.
//  Copyright © 2018 trying-things. All rights reserved.
//

import UIKit

class HelperInCallViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(self.goBack(_:)), name: .WebRTC_Close, object: nil)
    }
    
    @IBAction func pressHangUp(_ sender: Any) {
        FirebaseWrite.helperRemovesCallRequest()
        FirebaseObservers.stopHelperListeningForCallResponse()
        NotificationCenter.default.post(Notification(name: .CloseWebRTC))
        if let finishTime = FirebaseAuth.finishTime {
            let now = Date()
            if finishTime > now {
                FirebaseWrite.addHelperToDatabase()
            } else {
                print("Helper chose to finish at \(finishTime)")
            }
        }
    }
    
    @objc func goBack(_ notification: Notification) {
        FirebaseWrite.removeHelperFromDatabase()
        FirebaseObservers.stopListeningForAnxious()
        FirebaseWrite.helperRemovesCallRequest()
        FirebaseObservers.stopHelperListeningForCallResponse()
        NotificationCenter.default.post(Notification(name: .CloseWebRTC))
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "ViewController")
        self.present(viewController, animated: false, completion: nil)
        if let finishTime = FirebaseAuth.finishTime {
            let now = Date()
            if finishTime > now {
                FirebaseWrite.addHelperToDatabase()
            } else {
                print("Helper chose to finish at \(finishTime)")
            }
        }
        FirebaseObservers.listenForAnxious()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
