//
//  AnxiousInCallViewController.swift
//  Appxiety
//
//  Created by trying-things on 01/07/2018.
//  Copyright © 2018 trying-things. All rights reserved.
//

import UIKit

class AnxiousInCallViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(self.goBack(_:)), name: .WebRTC_Close, object: nil)
    }
    
    @IBAction func pressHangUp(_ sender: Any) {
        FirebaseWrite.removeAnxiousFromDatabase()
        FirebaseWrite.anxiousRemovesAllCallRequests()
        FirebaseObservers.stopAnxiousListeningForCallRequest()
        NotificationCenter.default.post(Notification(name: .CloseWebRTC))
        
    }
    
    @objc func goBack(_ notification: Notification) {
        FirebaseWrite.removeAnxiousFromDatabase()
        FirebaseWrite.anxiousRemovesAllCallRequests()
        FirebaseObservers.stopAnxiousListeningForCallRequest()
        NotificationCenter.default.post(Notification(name: .CloseWebRTC))
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "ViewController")
        self.present(viewController, animated: false, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
