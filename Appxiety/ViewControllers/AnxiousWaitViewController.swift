//
//  AnxiousWaitViewController.swift
//  Appxiety
//
//  Created by trying-things on 01/07/2018.
//  Copyright © 2018 trying-things. All rights reserved.
//

import UIKit

class AnxiousWaitViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.webRTC_Connected(_:)), name: .WebRTC_Connected, object: nil)
        
        
        FirebaseWrite.addAnxiousToDatabase()
        FirebaseObservers.anxiousListenForCallRequest()
        
        
    }
    
    @IBAction func pressBack(_ sender: Any) {
        FirebaseWrite.removeAnxiousFromDatabase()
        FirebaseWrite.anxiousRemovesAllCallRequests()
        FirebaseObservers.stopAnxiousListeningForCallRequest()
        NotificationCenter.default.post(Notification(name: .CloseWebRTC))
    }
    
    
    @objc func webRTC_Connected(_ notification: Notification) {
        print("AppDelegate.webRTC_Connected received connected notification")
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let anxiousInCallViewController = storyboard.instantiateViewController(withIdentifier: "AnxiousInCallViewController")
        self.present(anxiousInCallViewController, animated: false, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
