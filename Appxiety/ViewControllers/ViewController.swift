//
//  ViewController.swift
//  Appxiety
//
//  Created by trying-things on 01/07/2018.
//  Copyright © 2018 trying-things. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
             NotificationCenter.default.addObserver(self, selector: #selector(self.webRTC_Connected(_:)), name: .WebRTC_Connected, object: nil)
    }
    
    @objc func webRTC_Connected(_ notification: Notification) {
        print("AppDelegate.webRTC_Connected received connected notification")
      
        if let myID = FirebaseAuth.myID {
            if let helperID = FirebaseAuth.helperID {
                if myID == helperID {
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let helperInCallViewController = storyboard.instantiateViewController(withIdentifier: "HelperInCallViewController")
                    self.present(helperInCallViewController, animated: false, completion: nil)
                }
            }
        }
        
       
    }
    
}

