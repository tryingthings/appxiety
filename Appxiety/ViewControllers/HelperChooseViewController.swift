//
//  HelperChooseViewController.swift
//  Appxiety
//
//  Created by trying-things on 01/07/2018.
//  Copyright © 2018 trying-things. All rights reserved.
//

import UIKit

class HelperChooseViewController: UIViewController {

    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()

             NotificationCenter.default.addObserver(self, selector: #selector(self.webRTC_Connected(_:)), name: .WebRTC_Connected, object: nil)
    }
    
    @IBAction func pressStart(_ sender: Any) {
        let segmentIndex = segmentedControl.selectedSegmentIndex
        let selectedTime = segmentedControl.titleForSegment(at: segmentIndex)
        
        var minutes = Int()
        
        switch  selectedTime {
        case "5 Min":
            minutes = 5
        case "15 Min":
            minutes = 15
        case "1 Hr":
            minutes = 60
        case "24 Hrs":
            minutes = 1440
        default:
            print("HelperChooseViewController.pressStart().switch couldn't find a case for \(String(describing: selectedTime)) when converting title to minutes")
        }
        
        FirebaseWrite.helperFinishTime(for: minutes)
        FirebaseWrite.addHelperToDatabase()
        FirebaseObservers.listenForAnxious()
        
    }
    
    @IBAction func pressOffline(_ sender: Any) {

        FirebaseWrite.removeHelperFinishTime()
        
        FirebaseWrite.removeHelperFromDatabase()
        FirebaseObservers.stopListeningForAnxious()
        FirebaseWrite.helperRemovesCallRequest()
        FirebaseObservers.stopHelperListeningForCallResponse()
        NotificationCenter.default.post(Notification(name: .CloseWebRTC))
    }
    
    @objc func webRTC_Connected(_ notification: Notification) {
        print("AppDelegate.webRTC_Connected received connected notification")
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let helperInCallViewController = storyboard.instantiateViewController(withIdentifier: "HelperInCallViewController")
        self.present(helperInCallViewController, animated: false, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
