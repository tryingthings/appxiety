//
//  FirebaseReferences.swift
//  Appxiety
//
//  Created by trying-things on 01/07/2018.
//  Copyright © 2018 trying-things. All rights reserved.
//

import UIKit
import FirebaseDatabase

class FirebaseReferences: NSObject {

    // Make shortcuts for locations in the database
    static let callRequests = Database.database().reference().child("callRequests")
    static let anxiousWaiting = Database.database().reference().child("anxiousWaitingForHelpers")
    static let helpersWaiting = Database.database().reference().child("helpersWaitingForAnxious")
    static let helperFinishTime = Database.database().reference().child("helperFinishTime")
    
    static func setup() {
        callRequests.onDisconnectRemoveValue()
        anxiousWaiting.onDisconnectRemoveValue()
        helpersWaiting.onDisconnectRemoveValue()
    }
}

