//
//  SwiftyJSON.swift
//  Appxiety
//
//  Created by trying-things on 01/07/2018.
//  Copyright © 2018 trying-things. All rights reserved.
//

import UIKit
import SwiftyJSON
import FirebaseDatabase
import WebRTC

class SwiftyJSON: NSObject {
    
    // Send notifications to trigger WebRTC actions when the database receives changes
    func decodeSnapshot(_ message: JSON) {
        let type = message["type"].stringValue
        switch type {
        case "offer":
            print("SwiftyJSON.decodeSnapshot(): Received offer ...")
            let offer = RTCSessionDescription(
                type: RTCSessionDescription.type(for: type),
                sdp: message["sdp"].stringValue)
            NotificationCenter.default.post(name: .WebRTC_Offer, object: offer)
        case "answer":
            print("SwiftyJSON.decodeSnapshot(): Received answer ...")
            let answer = RTCSessionDescription(
                type: RTCSessionDescription.type(for: type),
                sdp: message["sdp"].stringValue)
            NotificationCenter.default.post(name: .WebRTC_Answer, object: answer)
        case "candidate":
            print("SwiftyJSON.decodeSnapshot(): Received ICE candidate ...")
            let candidate = RTCIceCandidate(
                sdp: message["ice"]["candidate"].stringValue,
                sdpMLineIndex: message["ice"]["sdpMLineIndex"].int32Value,
                sdpMid: message["ice"]["sdpMid"].stringValue)
            NotificationCenter.default.post(name: .WebRTC_Candidate, object: candidate)
        case "close":
            print("SwiftyJSON.decodeSnapshot(): Peer closed")
            NotificationCenter.default.post(name: .WebRTC_Close, object: nil)
        default:
            return
        }
    }
}
