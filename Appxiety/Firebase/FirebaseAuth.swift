//
//  FirebaseAuth.swift
//  Appxiety
//
//  Created by trying-things on 01/07/2018.
//  Copyright © 2018 trying-things. All rights reserved.
//

import UIKit
import FirebaseAuth

class FirebaseAuth: NSObject {
   
    // Store my anonymous ID here
    static var myID: String?
    // Store my token for push notifications here
    static var myPushToken: String?
    // When a helper chooses an anxious person, store the anxious ID
    static var anxiousID: String?
    // When an anxious person chooses an helper request to accept, store the helper ID here
    static var helperID: String?
    // Store the time helper wants to stop receiving calls
    static var finishTime: Date?
    
    
    static func signInAnonymously() {
        // Automatically give an ID of random letters/numbers to the user
        Auth.auth().signInAnonymously() { (authResult, error) in
            if error == nil {
                if let user = authResult?.user {
                    let isAnonymous = user.isAnonymous  // true
                    let uid = user.uid // eg. EGnhg3jvumTbXBiraDv0Tq142KK2
                    print("User ID \(uid) is anonymous: \(isAnonymous)")
                    myID = uid
                }
            } else {
                print("Firebase Auth error: \(String(describing: error))")
            }
        }
    }
}
