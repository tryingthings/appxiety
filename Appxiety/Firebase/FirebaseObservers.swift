//
//  FirebaseObservers.swift
//  Appxiety
//
//  Created by trying-things on 02/07/2018.
//  Copyright © 2018 trying-things. All rights reserved.
//

import UIKit


class FirebaseObservers: NSObject {
    
    static func listenForAnxious() {
        print("Listening for anxious")
        // Look for anxious people online and waiting to talk
        FirebaseReferences.anxiousWaiting.observe(.value, with: { (snapshot) in
            // At least one person is online if snapshot exists
            if snapshot.exists() {
                // Check that a call request hasn't been made already
                if FirebaseAuth.anxiousID == nil {
                    // Choose one person to connect to
                    // Available methods: "random"
                    let anxiousID: String? = FirebaseSnapshots.chooseAnxious(method: "random", snapshot: snapshot)
                    if anxiousID != nil {
                        // Save chosen anxious ID
                        FirebaseAuth.anxiousID = anxiousID
                        // Set value to AnxiousID/HelperID = 'request' in database
                        FirebaseWrite.helperSendsCallRequest()
                        print("Chosen anxious ID = \(String(describing: anxiousID))")
                        // Listen for a response
                        helperListenForCallResponse()
                    }
                }
            } else {
                print("No anxious people online")
            }
        })
    }
    
    static func stopListeningForAnxious() {
        print("Stopped listening for anxious")
        FirebaseReferences.anxiousWaiting.removeAllObservers()
    }
    
    static func anxiousListenForCallRequest() {
        print("Listening for call request")
        // Look for call requests from helpers choosing me
        guard let myID = FirebaseAuth.myID else {
            print("myID is nil")
            return
        }
        FirebaseReferences.callRequests.child(myID).observe(.value, with: { (snapshot) in
            // There's at least one call request if snapshot exists
            if snapshot.exists() {
                // Check that a call request hasn't been made already
                if FirebaseAuth.helperID == nil {
                    // Choose one call request to accept
                    // Available methods: "random"
                    let helperID = FirebaseSnapshots.chooseHelper(method: "random", snapshot: snapshot)
                    // Save chosen helper ID
                    FirebaseAuth.helperID = helperID
                    FirebaseAuth.anxiousID = myID
                    // Set value to AnxiousID/HelperID/ 'accepted' in database
                    FirebaseWrite.anxiousAcceptsCallRequest()
                    
                    print("Chosen helper ID = \(String(describing: helperID))")
                }
            } else {
                print("No helpers have chosen me")
            }
        })
    }
    
    static func stopAnxiousListeningForCallRequest() {
        print("Stopped listening for call request")
        guard let myID = FirebaseAuth.myID else {
            print("myID is nil")
            return
        }
        FirebaseReferences.callRequests.child(myID).removeAllObservers()
    }
    
    static func helperListenForCallResponse() {
        print("Listening for call response")
        guard let anxiousID = FirebaseAuth.anxiousID else {
            print("anxious is nil")
            return
        }
        guard let myID = FirebaseAuth.myID else {
            print("myID is nil")
            return
        }
        // Listen for a response to call request from anxious person
        FirebaseReferences.callRequests.child(anxiousID).child(myID).observe(.value, with: { (snapshot) in
            if snapshot.exists() {
                let response = FirebaseSnapshots.callResponse(snapshot: snapshot)
                print(response)
                if response == "accepted" {
                    let firebase = Firebase.shared
                    FirebaseAuth.anxiousID = anxiousID
                    FirebaseAuth.helperID = myID
                    firebase.sender = myID
                    firebase.receiver = anxiousID
                    NotificationCenter.default.post(Notification(name: .StartWebRTC))
                }
            } else {
                print("Call request no longer exists")
            }
        })
    }
    
    static func stopHelperListeningForCallResponse() {
        print("Stop listening for call response")
        guard let myID = FirebaseAuth.myID else {
            print("myID is nil")
            return
        }
        guard let anxiousID = FirebaseAuth.anxiousID else {
            print("anxiousID is nil")
            return
        }
        if FirebaseAuth.anxiousID != nil {
            FirebaseReferences.callRequests.child(anxiousID).child(myID).removeAllObservers()
        }
    }
    
    
    
    
    
}
