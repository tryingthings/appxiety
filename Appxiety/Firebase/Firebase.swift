//
//  Firebase.swift
//  Appxiety
//
//  Created by trying-things on 01/07/2018.
//  Copyright © 2018 trying-things. All rights reserved.
//

import UIKit
import FirebaseDatabase
import SwiftyJSON

class Firebase: NSObject {
    
    public static let shared = Firebase()
    
    // When a match is made, each person's ID is saved here.
    // They help make unique database references in setup() below.
    var sender = String()
    var receiver = String()
    
    // Database locations for setting up a call are used in WebRTC.swift
    var callReceiver: DatabaseReference?
    var callSender: DatabaseReference?
    
    func setup() {
        // Add database shortcuts for setting up a new call with each person's ID.
        callReceiver = Database.database().reference().child("Call/\(receiver)")
        callSender = Database.database().reference().child("Call/\(sender)")
        // Remove from database when disconnected (ie. hang up, network issue)
        callSender?.onDisconnectRemoveValue()
        callReceiver?.onDisconnectRemoveValue()
        negotiateConnection()
    }
    
    func negotiateConnection() {
        // Listen for changes in the database
        callReceiver?.observe(.value, with: { (snapshot) in
            // If no changes are found, do nothing
            guard snapshot.exists() else {
                print("Firebase.negotiateConnection(): snapshot doesn't exist")
                return
            }
            // Print changes to console
            print("Firebase.negotiateConnection(): found \(snapshot.value ?? "no snapshot")")
            
            // Convert snapshot to JSON
            let message = JSON(snapshot.value!)
            // Use SwiftyJSON.swift to get the content
            let swiftyJSON = SwiftyJSON()
            swiftyJSON.decodeSnapshot(message)
        })
    }
    
    func stopNegotiatingConnection() {
        callReceiver?.removeAllObservers()
        callReceiver?.removeValue()
        callSender?.removeValue()
    }
}
