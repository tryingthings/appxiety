//
//  FirebaseWrite.swift
//  Appxiety
//
//  Created by trying-things on 01/07/2018.
//  Copyright © 2018 trying-things. All rights reserved.
//

import UIKit

class FirebaseWrite: NSObject {

    // Add ID when helper is waiting for anxious people to call
    static func addHelperToDatabase() {
        guard let myID = FirebaseAuth.myID else {
            print("myID is nil")
            return
        }
        guard let myPushToken = FirebaseAuth.myPushToken else {
            print("myPushToken is nil")
            return
        }
        FirebaseReferences.helpersWaiting.child(myID).setValue("\(myPushToken)")
    }
    
    // Remove ID when helper doesn't want to receive calls
    static func removeHelperFromDatabase() {
        guard let myID = FirebaseAuth.myID else {
            print("myID is nil")
            return
        }
        FirebaseReferences.helpersWaiting.child(myID).removeValue()
    }
    
    // Add ID when anxious person is waiting for a helper
    static func addAnxiousToDatabase() {
        guard let myID = FirebaseAuth.myID else {
            print("myID is nil")
            return
        }
        FirebaseReferences.anxiousWaiting.child(myID).setValue("\(Date())")
    }
    
    // Remove ID when anxious doesn't want to make a call
    static func removeAnxiousFromDatabase() {
        guard let myID = FirebaseAuth.myID else {
            print("myID is nil")
            return
        }
        FirebaseReferences.anxiousWaiting.child(myID).removeValue()
    }
    
    // Helper sees an anxious person is online and sends their ID to them
    static func helperSendsCallRequest() {
        guard let anxiousID = FirebaseAuth.anxiousID else {
            print("anxiousID is nil")
            return
        }
        guard let myID = FirebaseAuth.myID else {
            print("myID is nil")
            return
        }
        FirebaseReferences.callRequests.child(anxiousID).child(myID).setValue("request")
        
        removeHelperFromDatabase()
    }
    
    // Helper stops showing their ID to anxious person
    static func helperRemovesCallRequest() {
        guard let anxiousID = FirebaseAuth.anxiousID else {
            print("anxiousID is nil")
            return
        }
        guard let myID = FirebaseAuth.myID else {
            print("myID is nil")
            return
        }
        FirebaseReferences.callRequests.child(anxiousID).child(myID).removeValue()
        FirebaseAuth.anxiousID = nil
    }
    
    // All helper IDs sent to an anxious person are removed
    static func anxiousRemovesAllCallRequests() {
        guard let myID = FirebaseAuth.myID else {
            print("myID is nil")
            return
        }
        FirebaseReferences.callRequests.child(myID).removeValue()
        FirebaseAuth.helperID = nil
    }
    
    // Tell helper that they've received their ID and want to talk
    static func anxiousAcceptsCallRequest() {
        guard let myID = FirebaseAuth.myID else {
            print("myID is nil")
            return
        }
        guard let helperID = FirebaseAuth.helperID else {
            print("helperID is nil")
            return
        }
        FirebaseReferences.callRequests.child(myID).child(helperID).setValue("accepted")
        let firebase = Firebase.shared
        firebase.sender = myID
        firebase.receiver = helperID
        NotificationCenter.default.post(Notification(name: .StartWebRTC))
    }
    
    // Save what time helper wants to go offline
    static func helperFinishTime(for minutes: Int) {
        switch minutes {
        // 1 is for testing. Less time to wait! :)
        case 1:
            print("1")
            var finishTime = Date()
            finishTime += 1 * 60
            print(finishTime)
            if let myID = FirebaseAuth.myID {
                FirebaseReferences.helperFinishTime.child(myID).setValue("\(finishTime)")
                FirebaseAuth.finishTime = finishTime
            }
        case 5:
            print("5")
            var finishTime = Date()
            finishTime += 5 * 60
            print(finishTime)
            if let myID = FirebaseAuth.myID {
                FirebaseReferences.helperFinishTime.child(myID).setValue("\(finishTime)")
                FirebaseAuth.finishTime = finishTime
            }
        case 15:
            print("15")
            var finishTime = Date()
            finishTime += 15 * 60
            print(finishTime)
            if let myID = FirebaseAuth.myID {
                FirebaseReferences.helperFinishTime.child(myID).setValue("\(finishTime)")
                FirebaseAuth.finishTime = finishTime
            }
        case 60:
            print("60")
            var finishTime = Date()
            finishTime += 60 * 60
            print(finishTime)
            if let myID = FirebaseAuth.myID {
                FirebaseReferences.helperFinishTime.child(myID).setValue("\(finishTime)")
                FirebaseAuth.finishTime = finishTime
            }
        case 1440:
            print("1440")
            var finishTime = Date()
            finishTime += 1440 * 60
            print(finishTime)
            if let myID = FirebaseAuth.myID {
                FirebaseReferences.helperFinishTime.child(myID).setValue("\(finishTime)")
                FirebaseAuth.finishTime = finishTime
            }
        default:
            print("FirebaseWrite.saveHelperTimes() couldn't find a case for \(minutes)")
        }
        
    }
    
    // Remove helper's time to go offline 
    static func removeHelperFinishTime() {
        if let myID = FirebaseAuth.myID {
            FirebaseReferences.helperFinishTime.child(myID).removeValue()
        }
    }
    
}
