//
//  FirebaseSnapshots.swift
//  Appxiety
//
//  Created by trying-things on 02/07/2018.
//  Copyright © 2018 trying-things. All rights reserved.
//

import UIKit
import FirebaseDatabase

class FirebaseSnapshots: NSObject {
    
    
    static func chooseAnxious(method: String, snapshot: DataSnapshot) -> String {
        if method == "random" {
            let anxiousID = chooseRandomID(snapshot: snapshot)
            return anxiousID
        }
        return ""
    }
    
    static func chooseHelper(method: String, snapshot: DataSnapshot) -> String {
        if method == "random" {
            let helperID = chooseRandomID(snapshot: snapshot)
            return helperID
        }
        return ""
    }
    
    static func chooseRandomID(snapshot: DataSnapshot) -> String {
        // Save all  user IDs here
        var userIDs = [String]()
        
        // Convert the database snapshot to a dictionary
        if let snapshotDict = (snapshot.value as? [String : String]) {
            // Make a list of all user IDs
            for (userID, _) in snapshotDict {
                userIDs.append(userID)
            }
            if userIDs.count == 1 {
                return userIDs[0]
            } else {
                if userIDs.count > 1 {
                    let randomNumber = Int.random(in: 0 ..< userIDs.count)
                    // Find the randomly chosen ID
                    let userID = userIDs[randomNumber]
                    return userID
                }
            }
        }
        return ""
    }
    
    static func callResponse(snapshot: DataSnapshot) -> String {
        // Eg. "request", "accepted"
        if let response = snapshot.value as? String {
            return response
        }
        return ""
    }
    
    
    static func helperFinishTime(snapshot: DataSnapshot) {
        if let dateString = snapshot.value as? String {
            print(dateString)
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss xxxxx"
            let finishDate = dateFormatter.date(from: dateString)
            print(finishDate!)
            
        }
    }
    
    @objc static func goOffline() {
        FirebaseWrite.removeHelperFinishTime()
        FirebaseWrite.removeHelperFromDatabase()
        FirebaseObservers.stopListeningForAnxious()
    }

}
